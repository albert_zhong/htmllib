from .Tool import *


class HtmlStyleSheet():
    def __init__(self, content='', **args):
        self.content = content
        self.name = 'style'
        self.args = args
        self.setup()

    def setup(self):
        arg_span = dictToArgs(self.args)
        self.code = f'<{self.name}{arg_span}>{self.content}\n</{self.name}>'

    def render(self):
        self.web_lines = loadWebLines()
        head_line = getPos('/body')
        self.web_lines.insert(head_line, self.code)
        writeWebLines(self.web_lines)
    
    def addBlock(self, for_, block):
        self.block = '''%s{%s}\n'''%(for_, block)
        self.content += self.block
        self.setup()


class HtmlScript():
    def __init__(self, content='', **args):
        self.content = content
        self.name = 'script'
        self.args = args
        self.setup()

    def setup(self):
        arg_span = dictToArgs(self.args)
        self.code = f'<{self.name} {arg_span}>{self.content}\n</{self.name}>'
    
    def render(self):
        self.web_lines = loadWebLines()
        head_line = getPos('/body')
        self.web_lines.insert(head_line, self.code)
        writeWebLines(self.web_lines)


class HtmlComment():
    def __init__(self, content=''):
        self.content = content
        self.setup()
        self.render()

    def setup(self):
        self.code = f'<!--{self.content}-->'
    
    def render(self):
        self.web_lines = loadWebLines()
        head_line = getPos('/body')
        self.web_lines.insert(head_line, self.code)
        writeWebLines(self.web_lines)

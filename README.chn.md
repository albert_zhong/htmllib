# PyHtmlLib
PyHtmlLib是一个自由化的Python包。它可以用Python来构建静态HTML页面。它由PointCode Organization构建，并在PyPI上发布。我们因其三大优势而强烈推荐您使用：

1. 该项目由Python编写以确保其轻量化、可靠性以跨平台性。
2. 与其他HTML包相比，它在架构包装上更简单明了，开发也更为便利。
3. 为确保其跨域实用性，构建的是国际通用的HTML格式。

## 如何下载？


```command
pip install pyhtmllib
```


## 如何导入？


```python
from HtmlLib.HtmlFrame import *
from HtmlLib.HtmlWidget import *
```
# author: Albert Z. of PointCode Organization
# This is the calling test of this project.
# You can edit your own file on the basis of it.

from HtmlLib.HtmlFrame import *
from HtmlLib.HtmlWidget import *
from HtmlLib.HtmlExtention import *

web = WebPage()
web.setTitle('hi, HtmlLib')

HtmlComment('HtmlLib Example')

style = HtmlStyleSheet()
style.addBlock('#main', 'background-color: #eeeeee; padding: 50px;')
style.addBlock('li', 'display: table; margin: 0 auto; font-size: 20px; color: red;')
style.render()

div = Div(id='main')
h = Heading('Why to choose HtmlLib?', size=1, align='center')
ul = UnorderedList([NormalListItem('light and reliable', class_='item'), NormalListItem('easy and simple', class_='item'), NormalListItem('useful and international', class_='item')])

div.addItem(h)
div.addItem(ul)
div.render()

web.saveHtml('test.html')
web.openWeb()
